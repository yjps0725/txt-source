# CONTENTS

梗角色轉生太過頭了！  
ネタキャラ転生とかあんまりだ！  

作者： 音無　奏  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%A2%97%E8%A7%92%E8%89%B2%E8%BD%89%E7%94%9F%E5%A4%AA%E9%81%8E%E9%A0%AD%E4%BA%86%EF%BC%81.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/ts/%E6%A2%97%E8%A7%92%E8%89%B2%E8%BD%89%E7%94%9F%E5%A4%AA%E9%81%8E%E9%A0%AD%E4%BA%86%EF%BC%81.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/ts/out/%E6%A2%97%E8%A7%92%E8%89%B2%E8%BD%89%E7%94%9F%E5%A4%AA%E9%81%8E%E9%A0%AD%E4%BA%86%EF%BC%81.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/ts/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/ts/梗角色轉生太過頭了！/導航目錄.md "導航目錄")




## [龍の来訪](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA)

- [序](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00010_%E5%BA%8F.txt)
- [夢的結束](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00020_%E5%A4%A2%E7%9A%84%E7%B5%90%E6%9D%9F.txt)
- [梗角色轉生](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00030_%E6%A2%97%E8%A7%92%E8%89%B2%E8%BD%89%E7%94%9F.txt)
- [能力把握](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00040_%E8%83%BD%E5%8A%9B%E6%8A%8A%E6%8F%A1.txt)
- [枯草色的半森精靈](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00050_%E6%9E%AF%E8%8D%89%E8%89%B2%E7%9A%84%E5%8D%8A%E6%A3%AE%E7%B2%BE%E9%9D%88.txt)
- [主従契約](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00060_%E4%B8%BB%E5%BE%93%E5%A5%91%E7%B4%84.txt)
- [龍の従者](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00070_%E9%BE%8D%E3%81%AE%E5%BE%93%E8%80%85.txt)
- [風竜](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00080_%E9%A2%A8%E7%AB%9C.txt)
- [黒狼の牙](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00090_%E9%BB%92%E7%8B%BC%E3%81%AE%E7%89%99.txt)
- [幻想魔法](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00100_%E5%B9%BB%E6%83%B3%E9%AD%94%E6%B3%95.txt)
- [變化的鬼](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00110_%E8%AE%8A%E5%8C%96%E7%9A%84%E9%AC%BC.txt)
- [杜蘭ＶＳ娜哈特](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00120_%E6%9D%9C%E8%98%AD%EF%BC%B6%EF%BC%B3%E5%A8%9C%E5%93%88%E7%89%B9.txt)
- [空白の中の可能性](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00130_%E7%A9%BA%E7%99%BD%E3%81%AE%E4%B8%AD%E3%81%AE%E5%8F%AF%E8%83%BD%E6%80%A7.txt)
- [無言的心意](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00140_%E7%84%A1%E8%A8%80%E7%9A%84%E5%BF%83%E6%84%8F.txt)
- [小さな奇跡](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00150_%E5%B0%8F%E3%81%95%E3%81%AA%E5%A5%87%E8%B7%A1.txt)
- [間話　娜哈特的約定俗成](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00160_%E9%96%93%E8%A9%B1%E3%80%80%E5%A8%9C%E5%93%88%E7%89%B9%E7%9A%84%E7%B4%84%E5%AE%9A%E4%BF%97%E6%88%90.txt)
- [改稿完了に伴う報告](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00170_%E6%94%B9%E7%A8%BF%E5%AE%8C%E4%BA%86%E3%81%AB%E4%BC%B4%E3%81%86%E5%A0%B1%E5%91%8A.txt)
- [報酬](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00180_%E5%A0%B1%E9%85%AC.txt)
- [冒險者公會](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00190_%E5%86%92%E9%9A%AA%E8%80%85%E5%85%AC%E6%9C%83.txt)
- [交易都市の二大貴族](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00200_%E4%BA%A4%E6%98%93%E9%83%BD%E5%B8%82%E3%81%AE%E4%BA%8C%E5%A4%A7%E8%B2%B4%E6%97%8F.txt)
- [龍の逆鱗](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00210_%E9%BE%8D%E3%81%AE%E9%80%86%E9%B1%97.txt)
- [兩個異變](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00220_%E5%85%A9%E5%80%8B%E7%95%B0%E8%AE%8A.txt)
- [一言の忠義](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00230_%E4%B8%80%E8%A8%80%E3%81%AE%E5%BF%A0%E7%BE%A9.txt)
- [お風呂で始動](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00240_%E3%81%8A%E9%A2%A8%E5%91%82%E3%81%A7%E5%A7%8B%E5%8B%95.txt)
- [名為娜哈特的希望](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00250_%E5%90%8D%E7%82%BA%E5%A8%9C%E5%93%88%E7%89%B9%E7%9A%84%E5%B8%8C%E6%9C%9B.txt)
- [守護者們的驕傲](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00260_%E5%AE%88%E8%AD%B7%E8%80%85%E5%80%91%E7%9A%84%E9%A9%95%E5%82%B2.txt)
- [出陣](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00270_%E5%87%BA%E9%99%A3.txt)
- [魔族の少女](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00280_%E9%AD%94%E6%97%8F%E3%81%AE%E5%B0%91%E5%A5%B3.txt)
- [懲罰](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00290_%E6%87%B2%E7%BD%B0.txt)
- [回憶與異變](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00300_%E5%9B%9E%E6%86%B6%E8%88%87%E7%95%B0%E8%AE%8A.txt)
- [合成魔法](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00310_%E5%90%88%E6%88%90%E9%AD%94%E6%B3%95.txt)
- [各自的死闘](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00320_%E5%90%84%E8%87%AA%E7%9A%84%E6%AD%BB%E9%97%98.txt)
- [各自的死闘（２）](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00330_%E5%90%84%E8%87%AA%E7%9A%84%E6%AD%BB%E9%97%98%EF%BC%88%EF%BC%92%EF%BC%89.txt)
- [亂入竜](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00340_%E4%BA%82%E5%85%A5%E7%AB%9C.txt)
- [龍撃魔法](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00350_%E9%BE%8D%E6%92%83%E9%AD%94%E6%B3%95.txt)
- [尾聲](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/00360_%E5%B0%BE%E8%81%B2.txt)
- [【電子版限定新發表SS】追憶的娜哈特](00000_%E9%BE%8D%E3%81%AE%E6%9D%A5%E8%A8%AA/10010_%E3%80%90%E9%9B%BB%E5%AD%90%E7%89%88%E9%99%90%E5%AE%9A%E6%96%B0%E7%99%BC%E8%A1%A8SS%E3%80%91%E8%BF%BD%E6%86%B6%E7%9A%84%E5%A8%9C%E5%93%88%E7%89%B9.txt)

