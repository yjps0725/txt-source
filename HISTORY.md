# HISTORY

## 2020-01-23

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 20, add: 0 )

## 2020-01-22

### Epub

- [魔王学院の不適合者](syosetu_out/%E9%AD%94%E7%8E%8B%E5%AD%A6%E9%99%A2%E3%81%AE%E4%B8%8D%E9%81%A9%E5%90%88%E8%80%85) - syosetu_out
  <br/>( v: 4 , c: 123, add: 38 )
- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 20, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-01-20

### Epub

- [原千金未婚媽媽](girl_out/%E5%8E%9F%E5%8D%83%E9%87%91%E6%9C%AA%E5%A9%9A%E5%AA%BD%E5%AA%BD) - girl_out
  <br/>( v: 4 , c: 77, add: 77 )

## 2020-01-18

### Epub

- [世界最強の後衛　～迷宮国の新人探索者～](syosetu_out/%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7%E3%81%AE%E5%BE%8C%E8%A1%9B%E3%80%80%EF%BD%9E%E8%BF%B7%E5%AE%AE%E5%9B%BD%E3%81%AE%E6%96%B0%E4%BA%BA%E6%8E%A2%E7%B4%A2%E8%80%85%EF%BD%9E) - syosetu_out
  <br/>( v: 6 , c: 38, add: 18 )

## 2020-01-17

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 19, add: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 98, add: 0 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-01-15

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 18, add: 1 )
- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 98, add: 1 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-01-10

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 17, add: 1 )

## 2020-01-08

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 16, add: 3 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 1 )

## 2020-01-05

### Epub

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( v: 1 , c: 13, add: 3 )

### Segment

- [三度被龍輾死，我的轉生職人生活](user2/%E4%B8%89%E5%BA%A6%E8%A2%AB%E9%BE%8D%E8%BC%BE%E6%AD%BB%EF%BC%8C%E6%88%91%E7%9A%84%E8%BD%89%E7%94%9F%E8%81%B7%E4%BA%BA%E7%94%9F%E6%B4%BB) - user2
  <br/>( s: 2 )

## 2020-01-03

### Epub

- [狼は眠らない](syosetu_out/%E7%8B%BC%E3%81%AF%E7%9C%A0%E3%82%89%E3%81%AA%E3%81%84) - syosetu_out
  <br/>( v: 44 , c: 452, add: 452 )

## 2020-01-01

### Epub

- [萬年Ｄ等級的中年冒險者](syosetu_out/%E8%90%AC%E5%B9%B4%EF%BC%A4%E7%AD%89%E7%B4%9A%E7%9A%84%E4%B8%AD%E5%B9%B4%E5%86%92%E9%9A%AA%E8%80%85) - syosetu_out
  <br/>( v: 4 , c: 104, add: 104 )



