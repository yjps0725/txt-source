# CONTENTS

魔剣師の魔剣による魔剣のためのハーレムライフ  
由魔劍師的魔劍開始的為了魔劍的後宮人生  

作者： 伏(龍)  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E9%AD%94%E5%89%A3%E5%B8%AB%E3%81%AE%E9%AD%94%E5%89%A3%E3%81%AB%E3%82%88%E3%82%8B%E9%AD%94%E5%89%A3%E3%81%AE%E3%81%9F%E3%82%81%E3%81%AE%E3%83%8F%E3%83%BC%E3%83%AC%E3%83%A0%E3%83%A9%E3%82%A4%E3%83%95.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/h/%E9%AD%94%E5%89%A3%E5%B8%AB%E3%81%AE%E9%AD%94%E5%89%A3%E3%81%AB%E3%82%88%E3%82%8B%E9%AD%94%E5%89%A3%E3%81%AE%E3%81%9F%E3%82%81%E3%81%AE%E3%83%8F%E3%83%BC%E3%83%AC%E3%83%A0%E3%83%A9%E3%82%A4%E3%83%95.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/h/out/%E9%AD%94%E5%89%A3%E5%B8%AB%E3%81%AE%E9%AD%94%E5%89%A3%E3%81%AB%E3%82%88%E3%82%8B%E9%AD%94%E5%89%A3%E3%81%AE%E3%81%9F%E3%82%81%E3%81%AE%E3%83%8F%E3%83%BC%E3%83%AC%E3%83%A0%E3%83%A9.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/h/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/h/魔剣師の魔剣による魔剣のためのハーレムライフ/導航目錄.md "導航目錄")




## [序章](00000_%E5%BA%8F%E7%AB%A0)

- [世界的終結](00000_%E5%BA%8F%E7%AB%A0/00010_%E4%B8%96%E7%95%8C%E7%9A%84%E7%B5%82%E7%B5%90.txt)
- [世界的終結與開始之間](00000_%E5%BA%8F%E7%AB%A0/00020_%E4%B8%96%E7%95%8C%E7%9A%84%E7%B5%82%E7%B5%90%E8%88%87%E9%96%8B%E5%A7%8B%E4%B9%8B%E9%96%93.txt)

