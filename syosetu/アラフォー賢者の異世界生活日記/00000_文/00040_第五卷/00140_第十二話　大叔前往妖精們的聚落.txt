「『『『『嘔嘔嘔嘔嘔嘔嘔嘔嘔嘔嘔！』』』』」

大叔用使魔探查到的妖精聚落，是個飄散出腐臭味的屍山。

他把複製了那景象的魔法紙的圖像拿給聚集起來的村人們和長老看，但一如所料的，所有人都忍不住吐了。

太過可怕，而且會讓人感到絶望的那張複寫畫，就算不想也會讓人徹底感受到妖精的危險性。

就連四神教的祭司也因為那張淒慘的恐怖畫像而摀著嘴，一邊和涌上的嘔吐感奮鬥著，一邊對四神教的信仰起了疑心。唉，這也沒辦法吧。因為他明明聽說妖精是純粹無瑕的種族，結果卻完全相反，是個超危險的種族。

「唔噗！妖精⋯⋯居然是⋯⋯這麼邪惡的⋯⋯種族⋯⋯」
「不能被外表給蒙騙呢。這世界上有長得醜的好人，也有長得好看卻火大到讓人想殺了他的垃圾。儘管有些噁心，但這就是妖精的本質喔。很過分吧？」
「這、這實在是太過分了吧⋯⋯嘔⋯⋯」
「這⋯⋯可不只是⋯⋯有些⋯⋯的等級⋯⋯」

所有人在克制嘔吐感的同時也理解了。妖精是必須打倒的魔物，絶對不可能和人類以及其他種族共存。

比起只是為了玩樂而殺害其他生物的妖精，順從自然法則，為了活命而捕食其他生物的魔物還像樣多了。

「妖精到底是為了什麼存在的呢？魔物身為生物，為了生存必須捕食其他種族這我可以理解，可是妖精什麼都不做啊～頂多只會像虫子一樣幫忙傳遞花粉，他們也不是一直在做這件事，真的不知道他們是為了什麼而存在的⋯⋯不覺得他們簡直就像是找麻煩的代表性種族嗎？」
「『『『『嘔嘔嘔嘔嘔嘔嘔嘔嘔嘔嘔嘔嘔嘔嘔！』』』』」

大叔在徵求大家的認同，然而在場的人們都因為強烈的嘔吐感而無暇回應。

妖精的天敵頂多也只有「食妖精獸」，他們絶對不會靠近其他強大的魔物。

從小小的惡作劇到驚悚的殺戮行為，只要可以滿足他們享樂的目的，他們什麼都做，而且毫無惡意。從旁看來他們就是所謂的邪惡，然而對妖精來說他們只是普通的在玩罷了。

「真、真是瘋了⋯⋯這到底哪裡不邪惡了啊。」
「沒、沒錯⋯⋯怎麼、怎麼能容許這種殘忍的行為！」
「令人困擾的就是他們並不邪惡呢。人類中有時會出現藉由殺害同族來獲得快樂的人，可是妖精們絶對不會殺害同胞。從他們的角度看來，會跟同胞展開戰爭的人類才真的是瘋了吧。我在伊斯特魯的大圖書館看過的書上也有提到，他們有可能是在模仿人類⋯⋯一開始只是在模仿，程度卻加重了吧？」
「關於人類的所作所為的確是這樣沒錯，可是只是因為好玩就殺害可以溝通的對象？這些傢伙⋯⋯對於同族以外的一切不都很殘虐嗎！」
「是啊⋯⋯不只是人類，就連獸人族和精靈都會出現同族相爭的情形。可是妖精們絶對不會跟同族起衝突喔。以某方面而言可說是非常和平的種族吧，但是面對其他種族就不是這樣了。他們只覺得其他種族像是有趣的玩具吧。不過如果這拷問就是妖精們對人類的認知，那也太討厭了⋯⋯」

人類會一邊適應環境，一邊成立組織，根據狀況不同，也有可能會和其他組織作戰。會因為政治或宗教理念，或是單純的感情而引發戰爭，互相殘殺，影響擴大後也有可能會成為國家之間的戰爭。獸人族和精靈也有不少這類的鬥爭情形，在妖精們看來，應該覺得會與同族互相殘殺的他們很奇怪吧。

可是妖精只能生存在從遠古時候開始就沒有變化的環境下，由於這種有些封閉的生態特性，讓他們無法接受外在的刺激，精神未能有顯著的成長。就算浮現小小的疑問，那也是和小孩子的思考一樣單純的玩意。

結果他們便得到了「雖然伙伴之間要和平相處，但不是伙伴就OK了吧？」這欠缺思慮的結論，幼稚的腦袋只會往享樂的方向去思考吧。

他們是相當長壽的種族，但對於個體沒有什麼執著，就算同伴在自己眼前被殺了也沒有感覺。
由於以奇怪的方向適應了弱肉強食的法則，所以他們完全沒有所謂的復仇心。

他們也沒有憤怒這種強烈的感情，將一切都視為遊戲的延伸產物。真要說起來就是只有「享樂」這種感情的種族。

「簡單來說，只要把他們想成是任性得嚇人，自我中心又不受控，會天真的拿著刀子去殺害動物的小孩子就對了。只是他們手上的刀子是藏有強大力量的魔劍呢。」
「因為天真無邪，所以才殘忍。我覺得自己似乎理解其可怕之處了⋯⋯四神啊，為什麼⋯⋯」
「所謂的善惡，是要培育智能，以自己的思考方式來判斷事情對錯，可是妖精們沒有那種智能。所以他們就像是一直處在失控的狀態下。」

妖精沒有什麼執著。就算碰到了要殺害自己的人，就連這個殺害的行為他們都覺得是一種遊戲。是種因為過於單純，所以不懂得變通的種族。但就是這樣才惡劣。

至今為止都努力的在幫妖精說話的祭司，看到杰羅斯帶回的複寫畫後，自己過去相信的事物便完全粉碎了。大叔有些同情他。

「總之我要去處理掉妖精們了。你現在知道妖精的天真無邪不是人類所想的那種東西了吧？」
「我⋯⋯很清楚的了解了。妖精不是該被擁護的種族。可是這件事情要是被本國知道了，我會被抓去異端審判吧。」
「祭司大人⋯⋯你沒有錯。除了妖精的事情之外，你不是很認真的為村子盡心盡力了嗎。」
「沒錯！因為妖精而受傷時，要是沒有祭司大人治療大家，現在不知道死了多少人⋯⋯」
「會擁護那種東西是上面的傢伙太奇怪了！祭司大人沒有錯！」
「各、各位⋯⋯謝謝你們⋯⋯嗚⋯⋯」

排除妖精的事情，這個祭司為村民們鞠躬盡瘁，單純地在傳教而已吧。他是上層主教們決定下的可怜犧牲者。

村人們有好好地把祭司的努力看在眼裡。
受到村人們的鼓勵，祭司感動的落下淚來。

「不過為什麼會提出擁護妖精的說法呢？很會找麻煩耶。」
「這是因為⋯⋯在距今約五百年前，四神透過聖女下達了旨意。『守護身為神之子的妖精們，他們是在終將來臨之時，將成為神使的無瑕者』⋯⋯就算在本國，妖精們的惡作劇也是過分得令人看不下去，但不管像我們這樣的人如何懇求，上層也只會說『這是試練』而沒打算處理。所以被選為傳教士的時候我真的很高興，可是⋯⋯」
「沒想到這個國家也開始受到了妖精的危害⋯⋯是說主教們也不會出手處理吧？畢竟收到了神的旨意啊。」
「根據我聽說的消息，他們現在也遵循著神的旨意，強調『不可殺害妖精』。」

祭司似乎也留下了痛苦的回憶。無法處理妖精的危害，也沒辦法回應信眾的心意。
夾在現實與信仰之間，應該承受了很大的壓力吧。

「唉，我是魔導士，所以神的旨意跟我無關就是了。我會為了獲得素材展開大屠殺的。」
「是說妖精的素材到底是⋯⋯我不知道那可以用在哪些地方。」
「『妖精之珠』可以用在『魔力藥水』上，『妖精之翼』則是有提高風魔法屬性效能的效果。是製作魔導具時不可或缺的素材喔。」
「從沒聽過耶？你以前很常打倒妖精嗎？」
「也是。現在市面上流通的回復系魔法藥，就算品質好，也多半是些效果不上不下的東西。問題就是出在缺乏妖精的素材上吧。這是大賺一票的好時機⋯⋯就是這樣，那麼我現在要去殲滅他們了唷♪」
「『『『『你要現在去嗎？已經要入夜了喔！』』』』」

大叔只想趕快解決麻煩事。
看著乾脆的起身走到玄關前的杰羅斯，村民們都愣住了。

「等一下！從這個時間開始妖精們的行動會比較活躍，太危險了！」
「他們要是衝上來，我就會反過來解決他們的。所謂的飛蛾撲火啊。」
「⋯⋯不，所以我說⋯⋯」
「期待我的好消息吧。就這樣。」

說完這句話，大叔便立刻朝著妖精們的聚落出發了。

「沒、沒問題嗎⋯⋯」
「『『『『誰知道？』』』』」

可疑的大叔走出去後，村人們的心裡閃過一抹不安。

畢竟光看外觀，大叔實在不怎麼可靠。

因為穿著灰色的長袍⋯⋯


◇　◇　◇　◇　◇　◇　◇

「哎呀？怎麼了？伊莉絲小姐。」
「叔叔⋯⋯要是叔叔外出時，妖精們跑到村裡來了怎麼辦？」
「說得也是⋯⋯這樣防衛戰力只有伊莉絲小姐，有沒有什麼好東西呢⋯⋯」

在村長家前面等候的伊莉絲向大叔搭話，覺得她這話也有道理的大叔開始翻找道具欄。然後拿出了五把投擲小刀，以及一把刀刃厚重的軍用小刀。

五把投擲小刀都長得一樣，握柄部分嵌有「魔法石」

軍用小刀也一樣，不過從刀鞘裡拔出來之後，只見刀刃上刻著細小的魔法術式，可以看出這是把魔劍。

「這是⋯⋯」
「投擲小刀是『封縛之投劍』，簡單來說就是可以打造出結界的東西。另一把刀是『星之刨刀』，是可以砍斷靈體的無屬性攻擊武器。雖然有對妖精有利的屬性，不過無屬性的純魔力攻擊即使效果差了一點，但還是很有效，所以借你防身用。」
「投擲小刀是丟出去後就能發揮效力？」
「對，可以暫時封住目標，不過同系的無屬性魔力可以貫穿過去，所以用小刀切碎他們就好。要五把整套一起使用，小心不要搞錯使用方法了。」
「唔⋯⋯這不是最後的王牌嗎。希望不要有派上用場的機會啊～⋯⋯」

伊莉絲忽然覺得很不安。

杰羅斯的確很強。光是待在他身邊就讓人覺得安心，可是要兵分兩路的話，老實說真的很可怕。妖精這種程度的對手對於杰羅斯來說只是小意思吧。可是對於伊莉絲而言可是難搞的對手。

畢竟她雖然擁有所有魔法強化和抗性類型的技能，但強化身體的技能只有「俊足」和「強體」而已，而且兩者的等級都很低。而且因為是魔導士，效果實在無法期待。

由於沒有格闘戰相關的技能，她無論如何都對近身戰鬥留有一些不安。更何況妖精的體型很小，身手矯捷得難以瞄準。

「真的沒機會派上用場就好了吶⋯⋯不過我不否認在妖精中也算是特別野蠻的『薔薇妖精』可能會來這個村子裡。畢竟那玩意的活動範圍似乎很廣⋯⋯」
「不要說了，叔叔！我不想和那種妖精戰鬥啦──────！」
「唉，這也只是保險起見啦。而且如果是『薔薇妖精』，伊莉絲小姐也打得倒喔。外表看起來是美麗的小女孩就是了⋯⋯」
「我愈來愈不想跟那玩意戰鬥了喔！這不是叫我去殺小孩嗎！」
「只是外表看起來像小孩而已喔？首先，既然是以魔物為對手的傭兵，連這種妖精都打不倒是不行的吧？要是這是工作接到的委託要怎麼辦？」
「唔！」

外表看起來像小孩的魔物還不少。

這種時候也不能因為看起來是小孩就拒絶接下工作。更何況也有可能是直接來自公會的委託。這也會影響到階級的審查。不能隨便依個人喜好來選擇工作。

要是不接下一些人家討厭的工作，那才會一直提升不了階級。最慘的情況下說不定會失去傭兵的登錄資格。

「我會連那個『薔薇妖精』一起，把妖精們全都處理掉就是了啦。」
「叔叔⋯⋯你的良心不會痛嗎？」
「完全不會。可以若無其事的做出恐怖又噁心行為的小女孩，燒光也無所謂吧。我手上是有作為證據的畫啦，可是非常可怕喔？跟超驚悚恐怖片一樣喔？甚至需要打上馬賽克⋯⋯唔，光想就快吐了⋯⋯」
「我⋯⋯沒看過那種東西⋯⋯」
「⋯⋯⋯⋯你想看嗎？真的想看嗎？會有好一陣子無法吃肉喔？是淒慘到甚至會改變人的人生觀的東西⋯⋯我這是為了保險起見才確認，你真的想看？這我是有在想是不是該列為十八禁，所以才沒給你看的喔。」
「⋯⋯這、這麼慘嗎？真的？」
「真的⋯⋯用淒慘這個詞來形容都顯得太簡單了。看過的人全都吐到停不下來喔⋯⋯」

伊莉絲非常感謝大叔的顧慮。

「唉，既然手上有王牌在，我建議你趕快趁現在裝上去吧。雖然最好是不要用上，但還是要預防萬一。」
「叔叔⋯⋯你不是以嚇我為樂吧？」
「怎麼會。我可沒空開這種玩笑。迅速解決工作是我的原則呢。」
「還真是討厭的工作啊⋯⋯要殺害看起來像小女孩的妖精對吧？」
「盜賊和黑社會的人就算被殺了多少都無所謂，可是那裡的屍體中也有女人跟小孩喔。得趕快殲滅他們呢。那麼我走了。」
「等等！」

大叔有些急迫，毫不猶豫的跑向位於村子東北方的道路。

他會這麼急，也就表示事情有這種程度的嚴重性。

「⋯⋯雖然不希望用上，但還是準備幾張王牌吧。畢竟要是有個萬一⋯⋯」

伊莉絲從道具欄中拿出了稱得上是自己王牌的幾個道具，裝備在手臂及脖子上。雖然因為是魔導士，所以是手鐲或項鍊這種裝飾型的裝備，不過這是目前的她所能拿出的最強裝備了。只是裡面也有幾個拋棄型的道具，要是用掉可就賠大了。

畢竟那不是可以在這個世界裡買到的東西。

『要是用上了這些東西你可要賠我喔！叔叔⋯⋯』

不過唯一可以做出這個裝備的人就是大叔。

伊莉絲心中盤算著如果必須用掉，之後就請大叔幫忙重新做一個。

生活很吃緊的傭兵工作，讓伊莉絲稍微變得成熟了一點。主要是往大嬸的方面就是了⋯⋯

她已經學到了若是不在財務上節約一點，就無法在這個世界生存下去這件事。


◇　◇　◇　◇　◇　◇　◇

大叔化為了風。

這個並不是譬喻，他也不是騎著「哈里・雷霆十三世」在飆車。
單純只是在全力奔跑而已，然而那速度快得很不尋常。
只是跑過街道邊的狹窄山路，就揚起了一片沙塵。而且那還是在大叔跑過的幾秒之後的事。
簡直像是人形的F1賽車，或是在新幹線旁邊用簡直看不見雙腳的超快速度奔馳而過的某個男主角。或是搭載有加速系統的改造人。

不，或許比較接近某個知名漫畫家的代表作品，有少女外型的機器人。

實際上他剛剛也撞飛了名為「山魔豬」的豬型魔物。

「哈哈哈⋯⋯我可是人類呢～可不會撞飛魔物喔～⋯⋯沒想到居然可以發揮出看不見腳的速度呢～啊哈哈⋯⋯」

然後大叔就開始逃避現實了。

來到這個世界後，要說杰羅斯有沒有全力奔跑過，那也只有剛轉生過來，在野外求生生活中要逃離凶暴的魔物大軍追趕的時候吧。那時以活下去為優先，根本無暇確認自己的體能。

大叔知道自己很不尋常，但完全無法掌握自己超乎常理到什麼程度。
大部分的對手他都能輕鬆打贏，甚至強壯到撞上山魔豬都沒受半點傷。根本是超人。
在平常的生活中用不上的體能，都透過自動發動的技能控制在一般的程度了吧。

要是真用這種誇張的體能度過日常生活，那才會造成許多困擾和危害吧。光是拿起陶瓷制的杯子，恐怕就會捏成粉末了。他甚至因此重新感受到「手下留情」的技能有多偉大。

而大叔在奔跑的途中不時會撞碎一些東西，顯然是妖精撞上了杰羅斯，因這衝擊而粉碎了。到了這地步，他根本是會奔跑的凶器。

大叔的體能簡直異常到會讓人想要張貼「小心，大叔無法緊急煞車」的警告標語的程度。而且對於大自然非常不友善。

不能長時間將村裡的防衛工作交給伊莉絲一個人，連忙趕往妖精棲息處的「魔力囤積處」的結果就是這樣。

這難以置信的狀況讓大叔絶望了。

「哈哈哈⋯⋯『要去異世界嗎？還是要放棄當人類呢？』總覺得沒得選擇，硬是被迫接受了這些事呢。普通⋯⋯這是多麼美妙的詞彙啊。」

現在他化為了與普通一詞最為遙遠的存在。

他知道自己是認真起來可以徹底蹂躪這世界的外來物，但沒想到居然到了這種程度。他一邊哀嘆著自己的遭遇，一邊高高跳了起來。

同時消去了自己的氣息，大叔和森林化為一體。
不過他還是無法避免在著地時掀起了一陣粉塵。

───

在微暗的森林中，大叔立刻發現了幾隻妖精。

『什麼？什麼？有什麼來了！』
『沒有東西⋯⋯到底是什麼呢？』
『有敵襲～～有敵襲～～♪』

忽然遭受襲擊讓妖精們嚇了一跳的樣子，但是因為大叔隱藏了自己的氣息，他們無法發現大叔。

而且正常來說應該會很慌亂吧，但是妖精們似乎被激發起了興致，興奮的像是在玩偵探遊戲的孩子。

『還沒到目的地就已經有六百只了啊⋯⋯沒想到妖精的數量有這麼多。是以「魔力囤積處」的魔力為苗床在繁殖嗎？看起來雖然是很夢幻美麗的景象，然而被光源照亮的周遭的狀況只能說是惡夢啊⋯⋯』

林木間聚集了許多的妖精，他們發出鮮艷的光芒來回飛舞的景象道盡了何謂奇幻。
不知道究竟有多少數量的妖精放出的光輝，那美麗的光照亮了森林。

要是樹木旁沒有被支解的動物屍體，大叔肯定會一直看著這景象吧。這是在美麗中帶著殘酷的光景。

而且在距離「魔力囤積處」有一段距離的地方就這樣了。愈靠近裡面，屍體的數量只會不斷增加。

「『伽瑪射線』×２０，威力全開。」

大叔放出了無數多重展開的積層魔法陣，將周圍的森林連同妖精一起燒毀。
屍體化為焦炭，蛋白質燃燒的討厭臭味飄散在森林中。

大叔從這裡開始進擊。

他邊走邊放出的「伽瑪射線」，讓妖精們連想逃都逃不掉，就這樣被消滅了。化為焦炭的森林樹木發出淒慘的聲音倒下，連帶波及其他樹木，揚起火花。

要是起風，餘燼就會燃燒起來，說不定會引發森林大火。
但是現在要以除掉妖精為優先。

『討厭的味道⋯⋯趕快搞定吧。也順便悼念一下犧牲者們吧⋯⋯』

持續放出魔法攻擊，自己的位置自然也會被妖精們給察覺。

可是「伽瑪射線」的攻擊是直線型的，要是擴大攻擊範圍，連整個集團都能夠一起殲滅。靠妖精的魔法抗性是無法承受的。也可以穿過妖精使出的魔法屏障。

單一個體的魔力抗性很高的話或許可以擋下，不過既然放出「伽瑪射線」的人是杰羅斯，實際上他們就已經是死路一條了。

而且和槍炮不同，他的攻擊完全沒有中斷過。

要說缺點的話就是射程受到重力的影響而變短了。不過他也沒打算做那種超遠距離攻擊，所以不構成問題。

雖然防止暴露在輻射下的術式也事先加進魔法術式的魔力變化術式中了，但這裡是異世界，說不定還是會有個什麼萬一，讓大叔十分在意。

『唔嗯～⋯⋯在「Sword And Sorcery」時雖然覺得這是伽瑪射線，但或許是類似伽瑪射線的某種東西？畢竟使出這個魔法後，經過一定的距離魔法就會變回原本的魔力⋯⋯是有什麼限制在嗎？』

正常來說伽瑪射線會一直前進，人類也不可能感受到射線前進的速度。雖然會受到重力的影響，但射線只是會有些許彎曲，所以只要掌握有效射程就可以解決這個問題。

變回魔力時可以察覺到擴散開來的魔力，說不定在這個世界裡，魔法的有效射程有一定的規律性，只要超出那個範圍就會立刻變回魔力。

不過就算知道這件事，對大叔來說也無關緊要。

雖然可以驗證這個異世界和「Sword And Sorcery」之間的差異性，但事到如今他也沒必要製作或改良其他魔法了，所以只把這件事情丟到腦中一隅。

阻止妖精造成的危害，讓哈薩姆村不會受到波及才是最重要的，他得盡量減少妖精的數量。

感覺就像是來驅除害虫的專家。

───

『我記得⋯⋯是在這裡吧？』

在陌生的森林中持續施放大範圍攻擊，等到看不見妖精的身影後，杰羅斯便立刻隱藏住自己的氣息，走向位於「魔力囤積處」的泉水。

儘管馬上就到了目的地，但那令人作嘔的濃厚腐臭味，讓大叔也不禁捏住了鼻子。回去後身上說不定會染上這股味道。

『你看你看，腐爛的眼珠♪』
『這邊是內臟，要再拿去丟在村子裡嗎？』
『比起那個，再帶小孩子來啦。我想要玩鬼捉人～♪』
『用槍在他們身上戳洞？還是要砍殺他們？活埋也不錯呢～』

有許多妖精聚集在周圍的樹木旁，在位於泉水中央的魔力囤積處上方補充消耗的魔力。附近被色彩斑斕的光芒照成一片夢幻的美景，然而散落在周遭的屍體和腐爛的肉塊驚悚的和這畫面完全無法連結在一起。若是覺得眼前的景象很美，那個人一定患有精神疾病吧。

也可以看到有新的妖精從魔力囤積處誕生的樣子，如果不讓這裡的魔力散開，妖精就會一直增加下去。

『「薔薇妖精」不在？上哪去了⋯⋯該不會⋯⋯』

從這大量的屍體看來，妖精們應該是分散至各地去誘拐獵物吧。

他們在半是好玩的殺害擄來的人們後，會再去尋找新的獵物。除了魔力消耗外他們不須飲食就能生存，所以可以不分晝夜的進行殘虐的遊戲。

而高階種族可以保有較多的魔力，一般來說活動範圍會比低階的妖精們更廣。

『只要消去這個魔力囤積處，他們的數量就會減少了吧⋯⋯只能使用殲滅魔法了。』

「魔力囤積處」有如流動在大地中的魔力洪流的腫瘤。魔力因為某些理由而滯留在一處，只要魔力仍持續流過來，就會不斷成長擴大。然後在某天達極限後破裂開來，使魔力擴散到世界中。

妖精和聖靈會住在這種魔力囤積處，增加同族的數量。要是無限的增加下去，魔力囤積處的魔力也總有一天會枯竭消失，但要是下方有龍穴就糟了。若是從龍脈流入無盡的魔力，魔力囤積處就不會消失，總有一天會從中誕生出強大的生物。被稱作「惡魔」或「聖獸」的魔物就是在這種地方誕生的。發展成那樣的話可就不只是妖精等級的騷動了。雖然必須連魔力囤積處一起淨化，問題是他不知道這個魔力囤積處的規模有多大。現在也沒時間慢慢調查，只能把周圍徹底炸掉了。

──喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔喔！

『唔哇～⋯⋯感覺快誕生出「惡魔」了呢⋯⋯唉，有這麼多的屍體在，這裡的瘴氣和怨念的濃度也很高吧～⋯⋯在那玩意誕生前解決這裡吧。嗯⋯⋯』

從魔力囤積處的內側產生了濃厚的瘴氣。

惡魔主要是從戰場等有許多生物喪命的魔力囤積處誕生出來的，和妖精一樣，會利用魔力囤積處來增加伙伴。在這過程中會虐殺人類等具有知性的生物，藉此提升周圍的瘴氣濃度，污染魔力囤積處。

惡魔本來和性質類似的妖精就是兩種相對的存在，所以甚至擁有能夠捕食妖精的能力。他們會將吃下的妖精之力用來創造同族或隷屬，進一步增加群體的數量。

此外他們本身也會成長為強大的個體，是有可能會在某天成為魔王種的魔物，但是沒想到妖精會創造出惡魔。

一般來說等到惡魔誕生後再與之戰鬥是故事主角該做的事，可是以現實層面來看根本不需要等那種玩意誕生。大叔立刻從潛意識解放出高密度的魔法術式，在掌心中展開。

出現了發出藍白色光芒的方塊型高密度壓縮魔法陣。

「『暴食之深淵』。」

他放出的高密度壓縮魔法陣抵達魔力囤積處上方後，包含在其中的魔法術式迅速啟動，形成了漆黑的球體。

『那是什麼？什麼什麼？』
『新的玩具？好強的魔力。』
『是什麼呢？感覺很有趣耶♪』

在玩弄屍體的妖精們對突然出現的黑色球體充滿了興趣。

『趁我還沒被波及之前，趕快離開這裡吧！』

大叔用全力逃離現場，心情就像是縱火犯。

在此同時，黑色的球體開始將周遭的東西一個個地吸入至內部壓縮。

妖精們無法抵抗這吸力，說著『被吸進去了～～～♪』、『呀啊～～～♪』，非常樂在其中的被吞噬了。

當然，魔力囤積處也無法幸免。無論是泉中的水，還是散落在周遭的屍體及骨頭都毫無分別的被吸了進去，把一切吞噬殆盡的同時，黑色的球體變得更大了。

然後超過了臨界點。

──轟隆隆隆隆隆隆隆隆隆隆隆隆隆隆隆！

大叔用全力逃到了安全距離外，看著妖精們的聚落被毀滅的樣子。

連周圍的樹木和土地都在瞬間消失了，緊接著便發生了大規模的爆炸，這強力的衝擊波使得山谷被剜掉一個廣大的範圍。

就算只有這物理性的衝擊波，也擁有妖精們無法承受的破壊力。

「唔喔喔喔喔喔喔喔喔喔喔喔喔喔喔！」

衝擊波擴散到周遭，大叔被自己使出的魔法副作用給波及了。

過剩的衝擊波將岩盤深深的挖出一個大坑，捲入了周遭所有的事物，在山谷中形成了一個巨大的隕石坑。

被衝擊波給震飛的大叔，好不容易才被沒倒下的大樹枝丫給勾住。

太慘了。

「⋯⋯『暴食之深淵』就有這種威力。我在那座森林裡用了很不得了的魔法啊⋯⋯」

忘不了的法芙蘭大深綠地帶。

被大群的哥布林持續追趕著，逃到的地方還好死不死是哥布林的大聚落。他因為壓力而沒多加思索，便順勢用了廣範圍殲滅魔法「暗之審判」

「暴食之深淵」是「暗之審判」的試做版本，也是強大的範圍魔法。

藉由超重力壓縮來吸收周圍的物質，逐漸膨脹。最後形成的黑洞會因為強大的重力自行毀滅，一口氣爆發開來，瞬間毀滅周遭的事物。

「暗之審判」則是藉由吸收魔物來形成重力場，同樣會自行毀滅，以毀滅時的威力粉碎敵人，不過以單發的效果而言，「暴食之深淵」的威力比較強。可是「暗之審判」能夠將大範圍的敵人全都化為重力場，變成火藥，藉此擴大有效範圍，所以影響的範圍十分廣大，以冷靜的觀點來看「暗之審判」影響範圍是比較大的。

由於有多少敵人就會產生多少的重力場，在敵人全數被消滅前攻擊絶對不會停下來，是一種無法調節的魔法，使用上也會讓人有些猶豫。

相較之下「暴食之深淵」可以任意操控攻擊的範圍。只是由於重力場崩壊所產生的副作用，使得受害範圍更為廣大。

儘管是只有一發的範圍魔法，但從那裡產生的衝擊波會對周圍產生極大的影響。

結果爆炸中心的周遭變得非常淒慘。

───

「嗚哇～⋯⋯⋯⋯⋯⋯慘不忍睹。」

魔力囤積處是消失了，但在那邊的蓊郁森林也一起被消滅了。

這還是大叔已經有克制威力的結果。

明明只要能消除「魔力囤積處」就好了，但是他無法防範隨之產生的副作用，讓受災範圍擴大了。

這點大叔也無法預測。因為在「Sword And Sorcery」使用時威力小多了。在現實中使用是相當危險的魔法。

『對邪神使用的時候也沒這麼大的威力，這⋯⋯有克制的情況下還這樣，要是認真使出這招的話會造成多大的損害啊？根本是廣範圍殲滅魔法的等級嘛。』

只是試著用了普通的殲滅魔法，卻發現那是個超級危險的魔法。
衝擊波應該把生息在這附近的妖精們全都消滅了吧。

畢竟這衝擊波的威力瞬間把自然界的魔力炸飛了出去，涌出的衝擊波和魔力化為的帶有破壊效果的大海嘯襲向了妖精們。

魔力衝擊波接連破壊了妖精的半魔力體，大規模的擴散出去。
雖然範圍比不上副作用的衝擊波，但說起來可以算是三度傷害了吧。

大叔身上冷汗直流。

「哎、哎呀，不小心搞砸了這也沒辦法。就裝作不知情吧。跟他們說我用了魔法，結果『魔力囤積處』就忽然爆炸，害我差點就死了好了⋯⋯哈哈哈⋯⋯唉～」

反正他們也不知道原因，只要蒙混過去硬是讓他們接受這說詞就好了。
大叔太過分了。
而且處理態度很隨便。

從「Sword And Sorcery」時期留下的習慣看來是治不好了。

因為大叔的失誤，讓哈薩姆村陷入了嚴重的缺水狀態，不過大叔炸出的隕石坑涌出了地下水，變成了一座湖，一年後成了豐沛的水源。

再過了兩百年後，這附近成了有名的王族專用度假勝地，長期以來都為了保護環境而有人在管理著。

而又過了三百五十年後，德魯薩西斯公爵寫下的年代紀錄書被人發現，這個湖是「大賢者」一時失誤所誕生的產物一事才因此公諸於世。這是被後世稱為「梅林之湖」的隕石坑所發生的故事。

到眾人知道真相為止，過了五百年以上的時光。


◇　◇　◇　◇　◇　◇　◇

杰羅斯前往妖精們的聚落（不對，這種情況下該說是巢穴嗎？）之後，沒事做的伊莉絲在村裡閑晃著。

她雖然算是被委任了保衛村子的任務，但是目前她並沒有感覺到妖精們的魔力。
就算看不到妖精的身影，也能感覺到他們的魔力，所以只要有妖精出現的話，她大概可以掌握住他們的數量及位置。

伊莉絲走在田畦中的小路上時，覺得忽然傳來了某個聲音。

「嗯⋯⋯？那是什麼⋯⋯小孩子？」

現在小孩子們都被藏在家中，不能外出。
她認為或許有妖精在，走向發出聲音的位置後卻看到了難以置信的景象。

那是牛浮在空中，腹部被撕裂、內臟被拉了出來的景象。而且周圍沒有任何人，唯一能感覺到的只有一鼻子的鐵銹味，是血的腥味。

「家、家畜異常虐殺現象？」

她的腦中浮現不祥的預感，擺出了備戰姿勢。
牛的周圍有高濃度的魔力，而這團魔力彷佛擁有意志，正在支解牛只。

『啊～啊，死掉了。不過沒關係，好像有新的玩具來了。』
「唔！『魔力彈』！」

察覺到危機，伊莉絲立刻朝著感覺到高濃度魔力的地方擊出魔力彈。

傳來了『呀啊！』的可愛聲音後，在那裡化為實體的不是小型的妖精，而是有著一頭紅髮，背上長有如血般鮮紅的鳳蝶翅膀的少女。

身上沒有穿著衣服，取而代之的是有像是植物的藤蔓纏繞在他的身體上。

「『薔薇妖精』⋯⋯」

薔薇妖精對著這麼說著的伊莉絲露出天真無邪的微笑。