﻿
「這裡是......外面嗎？」

我和莎莉婭，被羊轉移到現在還沒到過的森林之外。
在我眼前伸展的景色，是一片寬闊的草原，草只有到腳裸附近的長度。
我不自覺地把視線轉向在後面存在的『無盡的悲愛之森』。
現在想起來的話，我在那個森林中度過了一段漫長的時間......
是鍛煉了我的精神和肉體的地方。
......也是繞了一圈重新回到人類的地方。
發生了很多事呢，跟莎莉婭相遇的事，也是為像現在一樣地能活下去感到喜悅的場所呢......
抱著深厚的感概眺望著森林時，莎莉婭來到了旁邊。

「我，是第一次來到外面......。」

視線轉向莎莉婭，莎莉婭正在閉著眼睛，如同在感覺著什麼一樣。
對莎莉婭也是充滿回憶的地方吧，可以說是故鄉的地方呢。
互相對著森林有種各種各樣的感想。
我們度過一段無言的時間，不久我對莎莉婭說。

「那麼......該走了吧！」
「嗯！」

收到莎莉婭回答後，我和莎莉婭背對森林開始走了。
在不久之前拖鞋壞掉的我，用裸足在草原上走。
在森林的時候，地面硬硬的，像這麼多草生長的地方一點也沒有。
那個原因嗎，腳踩在草上感到癢癢的，禁不住笑了起來。

「怎麼了？」

對著那樣的我，莎莉婭感到不可思議地問了。

「不，什麼都沒有。」

我那樣回答，開始考慮今後的事。
首先，依靠澤阿納斯的記憶向著最近的城鎮移動。
這是最優先事項。
在到達街道之後，也要考慮一下是否對冒險者進行登錄。
不僅是澤阿納斯的記憶，在從神那得到的知識中，冒險者這個職業的存在也記載著。
之後，是我的力量吧。
根據羊的話，我的外表會作為技能『偽裝』的效果被表示的數值的程度被對方認識。
如果羊沒有說謊的話呢。
所以我才被迫戴著全面頭盔......
嗯，超礙事！
好想全力的破壞它。
首先，全面頭盔......
把它帶入幻想世界是不行的吧。
嘛，能脫下來所以就算了......
這也是那隻羊說的是真的話。
而且，如果我的容姿因為偽裝的效果而低下的話，我的力量有可能低下了。
這個，不好好地確認的話，不知道會發生什麼不好的事呢。
這個那個地考慮後，結果現在要怎麼辦也沒想到，我暫時切斷思考，與莎莉婭一起在草原中步行。

◇◇◇◆◆◆◇◇◇

離出發已經過去不少的時間。
我與莎莉婭，與誰都知道的生物相遇了。
那是──

「這，這傢夥......。」
「好可愛啊！」
「噗嚕嚕。」

──史萊姆。
不，不奇怪嗎？
眼前的史萊姆，可是自己說了『噗嚕嚕』哦？
是與史萊姆相似的不同生物嗎？
但是，眼前的生物，半透明的果凍狀，在草原上噗扭噗扭地跳著。
只是，眼和口和耳都沒有看到。
嗯，果然聽到『噗嚕嚕』是錯覺吧。

「噗嚕嚕！」
「不是錯覺啊！」

從哪裡發出聲音的啊！？
這個世界的史萊姆都是『噗嚕嚕』地叫著的嗎！？
......不，還沒有確定一定是史萊姆。
是不同的生物也有可能！
幸好我的有技能『上級鑒定』，現在立刻就使用吧。
那樣決定的我，馬上對在眼前噗扭噗扭地扭動的史萊姆發動了技能『上級鑒定』。

『史萊姆LV：88』

「等級好高！」

而且只是單純的史萊姆嗎！？
話說，史萊姆的等級都這麼高的嗎！？
確實與聰明猴子和阿庫洛狼，澤阿納斯之類的程度難以比擬的低。
但是......但是史萊姆有88也太奇怪了吧！？
對史萊姆異常的等級感到驚訝，突然史萊姆的身體開始一口氣壓縮。

「什，什麼？」

對突然的行動困惑時，史萊姆壓縮的身體一口氣解放，向著我突進了。

「哎喂！？」

因為突然的攻擊發出奇怪的聲音，但多虧了固有技能的『心眼』嗎？
史萊姆的活動看上去非常緩慢，富有餘裕地避開了。

「突然就攻擊嗎......。」

嘛，史萊姆也是魔物，原來就沒有不會攻擊這樣的保障......
想著一些無所謂的事時，忽然我把某件事想起來了。
那是，確認一下我的力量是否因為偽裝的效果而弱化了這件事。
為了確認那個，我眼前的史萊姆正適合。

「好......就要這隻史萊姆來稍微確認一下吧！」

一邊那樣說，我一邊從腰上把小黑（『憎惡漩渦的細劍』）和小白（『慈愛滿懷的細劍』）拔出，架起。
利用這隻史萊姆，確認我的力量到底變成怎樣了。
雖然使用技能進行攻擊也可以，但用通常攻擊來確認比較好吧。
那樣想的我，在史萊姆再次攻擊之前，從這邊發出了進攻。

「哈！」

一口氣向著史萊姆衝出去的我的感想是──

「好，好快？」

這句話說完了。
本來是想跑過去的，但只需一步，史萊姆就出現在眼前地一口氣地移動了。
而且，在我衝出去的瞬間，剛才我還站著的地面被挖去了一大塊。
我變得困惑，但因為已經到達了史萊姆的面前所以我只好攻擊，右手拿著黑色往上舉起，然後一口氣向著下方的史萊姆揮去。
咚隆！

「......。」

與可怕的衝擊音一起，眼前的掉落道具亂飛著。
......
......

「哎？」

不明白，我禁不住發出愚蠢的聲音。
剛才在史萊姆存在的地方，從那個地方開始約50m的前方的地面被挖去一大塊。
......

「......不想面對現實......！」

終於開始理解現狀的我，但越理解越想忘記自己剛才親手做的事。
也就是說，史萊姆因為我的一擊飛散了。
確實我是一口氣用黑色向著史萊姆揮去。
但是，決定沒有認真地揮下去。
只是抱著『輕輕地揮動造成一點傷害』這樣的心情揮動的。

「那個結果是這個嗎！？」

嗯，在跑出去的瞬間已經有種討厭的預感了啊！？
但是預想外啊！
威力太奇怪了啊！
這樣的話就能確定力量不能偽裝了啊！？
只是數值能偽裝嗎！？
我，認真的攻擊會怎麼樣啊！？
只是輕輕地就有這種威力？
......不想想像啊。
抱著頭，對自己的力量絕望的時候，莎莉婭的眼睛發亮著。

「誠一好厲害！雖然剛才的生物好可愛......。」

嗯，抱歉！
我也沒有殺的打算哦！
只是想稍微確認一下力量，之後立刻逃走而已！
沒有說謊哦！？
對浮現出一點悲傷的表情的莎莉婭，不尋常的罪惡感湧了出來。
真的，對不起......
在各種意義上累了，但突然掉落道具中的幾個化為光球上浮，突然就進入了我的裡面。

「什，什麼！？」

雖然禁不住那樣說了，但總覺得能明白。
恐怕，是史萊姆的status進入了我的體內吧。
為什麼不是平時的球狀而是一口氣進入體內這點就不知道了......
果然，進入我體內的光球是史萊姆的status，在腦內響起了聽慣的聲音。

「嗚～～......嘛，算了！」

既然殺了也沒辦法，回收掉落道具吧。

「首先是技能卡。」

那樣說著，我撿起了應該是技能卡的東西。

『技能卡『吸收』』――能習得技能『吸收』。

『技能卡『壓縮』』――能習得技能『壓縮』。

「嗚，嗯......？」

看著史萊姆的技能卡，我只能發出微妙的反應。
因為，是厲害嗎？還是不厲害嗎？也不太明白。
嘛，既然是史萊姆，效果應該是很樸質的吧。
抱著輕鬆的心情一個在點頭時，技能看化為光球，進入了我的體內。

『習得了技能『吸收』。習得了技能『壓縮』。』

對頭中響起的聲音無視，確認技能的效果。

『吸收』――吸收所有的東西，轉化為自己的力量。吃下去的東西不止營養，不留下任何地轉化為自己的力量。在受到傷害的時候發動，也能吸收傷害轉化為自己的力量。

『壓縮』――可以壓縮所有的東西。自己的身體和力量等，基本上什麼都可以壓縮。但是，在壓縮時，有接觸的必要。

「超作弊啊！」

在這以上我變強到底要幹什麼啊！？我要以什麼為目標啊！？
可是，技能『吸收』......
結合我的『心眼』使用的話，不是最強的組合嗎！？
各種意義上無敵了！

「哈......對已經習慣這種程度的作弊的自己覺得好可怕......。」

普通能入手作弊的話不是會很高興的嗎？
為什麼會這麼空虛呢？
只有我嗎？
我很奇怪嗎？
表面上，雖說種族是寫著人類，但已經漸漸遠離人類這方面即使討厭也感覺到......
吐出放棄的歎息，我向著下面的掉落道具的確認移動了。
然後，新撿起來的，是不太明白的噗扭噗扭的半透明物體。
非常讓人在意。
因為完全不明白用法，所以照例地發動『上級鑒定』。

『史萊姆果凍』――觸感良好的史萊姆的碎片。沒什麼特別的使用方法，能吃。

「史萊姆自身好差勁！」

明明技能那麼厲害！
為什麼本體的掉落道具那麼差勁啊！？
這個......
除了吃以外沒有能使用的方法了嗎？
好礙事啊......
但是，既然鑒定的結果出來了，就沒有辦法了吧。
一邊歎氣，一邊把史萊姆果凍放進道具箱裡。

「然後，接下來是？」

拿在手上的，是寶箱。

「好小......嘛，沒什麼大的東西在裡面吧。」

那樣想著，打開了寶箱。
在裡面的東西，可怕是放錢的袋子，還有靴子。

「靴子嗎......。」

該怎麼說呢，雖然不是瞄準的裝備品，但在裸足的時候出現實在太合時了。
取出來，是漂亮的蒼色的重視機能性的鞋子。
是用什麼素材做的就不知道了。
反正是史萊姆的裝備品，不會有什麼期待。

『鑒定』，確認效果。

『蒼之靴』――稀少級裝備品。可以稍微強化裝備者的敏捷力，可以在空中行進3步，能根據裝備者的尺寸變化。

「挺厲害的！」

雖然有點微妙，但意外的厲害呢！？
稍微強化我的敏捷力？
......會變成怎麼樣啊。
而且，為什麼只能3步不太明白，但多少能在空中移動。
這個，不是挺厲害的嗎？
可是......
雖然靴子入手是好，但想洗下腳，穿上襪子後再穿呢。
嘛，無所謂吧。
比起那個，讓莎莉婭穿吧。

「莎莉婭，不試著穿下這個嗎？」
「哎？可以嗎？」
「嗯，我沒有靴子也可以的。」

而且，在讓女孩子用裸足步行這件事上也感到了罪惡感。

「是嗎......那麼收下了！」

在精神地回答的莎莉婭的腳下，把靴子整齊地放下。
莎莉婭是因為第一次穿靴子而緊張嗎？慎重地穿著靴子。

「怎......麼樣？」

穿完靴子後，莎莉婭稍微有點害羞地抓著連衣裙的下擺，展現著靴子。
白色，美麗的肌膚配上蒼色的靴子，與莎莉婭的容姿驚人地相合著。

「非常適合哦！」

完全沒有說謊。
深紅的頭髮，白色的連衣裙。
還有腳上的蒼色的靴子。
雖然樸質，但看上去非常的漂亮。

「哎嘿嘿......謝謝！」

對著如花朵開發地呼出滿臉笑容的莎莉婭，自然地臉頰泛紅。
......嗚哇，我的臉絕對很紅......
雖然是非本意，但只有在這時候才對全面頭盔覺得感謝。
真的與大猩猩的時候的差距驚人呢。
......莎莉婭姑且不論，想著大猩猩的時候也可愛的我很奇怪嗎？
不愧，是擁有大猩猩媳婦的男人啊！

「咳咳......那麼，最後確認錢吧。」

我那樣說，把錢袋取出，確認裡面。
於是，裡面放著銀幣50枚。

「嗯～～......金錢的感覺變得有點奇怪了......。」

銀幣50枚應該是很多的。
明明只是隻史萊姆。
但是，因為打倒聰明猴子們得到的金錢更加多，所以沒有產生厲害的感覺吧。
這個，可能真的有點不妙啊......
可是，現在也沒有辦法。
只能在到達街道後再把金錢的感覺取回來了......
在吐出比平時更多的歎息的時候，突然在我的頭中響起了嘹亮的喇叭聲。

「這次又是什麼啊！？」

莎莉婭果然是沒有聽見嗎？對那樣叫著的我感到不可思議地歪著頭。
對嘹亮的喇叭聲的意義難以理解的時候，聽慣的聲音在頭中響起了。

『升級了！』
......
......
......

「哎！？」

