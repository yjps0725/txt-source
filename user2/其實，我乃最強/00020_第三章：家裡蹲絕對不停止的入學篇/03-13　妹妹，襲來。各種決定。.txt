入學的第二天，交了朋友。
對於原【啃老家裏蹲】的我來說是難以置信的成果。
但是，不高興。以提前退學為目標的我完全沒有希望。

「那麼，明天見。」

即使轉過身去，也被速攻阻止。

「去哪裡？」

「回宿舍房間吧。嗯……看，是那個。還沒選課。」

「那我也一起做吧。我也決定了。我覺得兩個人一起商量，效率會更好，可以做出合適的選擇。」

啊—說就說吧。
那我就說出一點帥氣的臺詞吧。

「我不打算配合妳。」

「當然不會對你想做的事情插嘴。我也是為了完成使命才進這個學院的。既然有該做的事情，我就不會配合你而妥協的。」

真了不起。嘛，那些進入精英學校的人都是這樣的吧。
就算在這吵架，伊利斯也許不會讓步。
我本來打算用轉筆來決定上課的事，最後決定踢掉它。

「那麼，要來我的房間嗎？」

「嗯。請多關照。」

然後——。


雖然心情比較輕鬆，但是仔細想想，這可是在自己的房間裡招待女孩子的一大活動。
當然，前世除了母親以外，沒有女人進入過。這個世界也只有女僕和夏爾。

一邊想著這樣可以嗎，一邊打開門。

「兄長大人，你回來了——哇！？」

為什麼會有夏爾？
我妹妹滿面笑容地迎接我，看到從我身後進來的伊莉絲，瘋狂地叫了一聲，加固了。

「那邊的人是……難道是戀人！？」

「朋友。」

「不愧是兄長。早就交到了很好的朋友了呢。啊—太好了。」

從心底安心的樣子，我是不是那麼被擔心呢？

「不過，兄長馬上就能找到五個或十個戀人了吧。我也是下定決心的時期到來了。」

妹妹繼續著各種吐槽的話語，睜著圓圓的眼睛。

「如果可能的話，我當第二夫人左右怎麼樣？」

「要瞄準當二夫人嗎？話說在這個國家重婚OK嗎？」

「雖然在制度上是不行的，但那畢竟是兄長大人，所以無論如何都要。」

嗯，不明白意思。
但是這樣啊。這傢伙還是從小就說「我想和你結婚」嗎？如果再過幾年「大哥什麼的很噁心吧？」像這樣咂著嘴的孩子……我希望你別來。我作為哥哥也要堅強起來。

「這作為兄妹之間的對話，是很普通嗎……？」

「大概吧。」

總之，給對我們兄妹的談話感到困惑的伊莉絲介紹了。

「這傢伙昨天認識的，今天交朋友了，伊利斯菲利亞。太長了，簡稱《伊利斯》就好。」

「有那樣的介紹方法嗎……。不過，如果是朋友的妹妹，名字可以省略。」

「那麼，這是我妹妹夏洛特。簡稱《夏爾》也可以。」

「承蒙介紹，我是兄長的妹妹夏洛特．芬菲斯。今後也請多多關照，伊莉斯小姐。」

夏爾拉起裙子，優雅地低下了頭。

「嗯，請多關照，夏爾。」

也沒有特別互相責備（當然），二人相處融洽。

「夏爾，我不知道有什麼事，請稍等一下。我們有應該做的事情。」

我馬上開始著手選課。
打開學校發放的冊子，在課堂一覽的頁面上轉動筆。

「等一下，哈特。你在幹什麼？」

「我決定上課了？」

「我給大家解說就是【無論選擇哪個，對兄長來說都是從容不迫的，所以沒有問題】。」

「對不起。我完全無法理解。哈特也有想做的事吧，既然進這個學院了。那麼就應該遵循那個慎重地決定授課。」

「兄長的目的不在學業。原本，像兄長那樣的有實力的人向誰學習什麼的前提就是錯誤的。」

「我承認哈特的實力很高。但是，即使是實力差的人，也應該有很多東西要學習。」

「因為兄長的實力超過了這種一般常識。」

「那，就到此為止了嗎……」

我把話題拋開不管，很輕鬆就好了，這個。

「突然在意學業以外的目的是什麼。能告訴我嗎？」

「很抱歉，就算是朋友也不過如此……」

夏爾神秘地搖頭。
嗯，真不愧是【提前退學】啊。夏爾知道啦。妳什麼時候知道的？
不過嘛，就算能理解我的想法，也說得太快了。

「夏爾啊，能幫我選一個適合我的課嗎？」

「哈！真是不勝榮幸。夏洛特，一定會回應兄長的期待。」

恭恭敬敬地低下了頭的夏爾的表演也正進行著。這傢伙在城裡接受著英才教育，像這樣放鬆是必要的。

於是，我躺在床上。
夏爾開始和伊莉斯熱心地交談，我開始在眼睛與耳朵貼上結界，收看動畫。
時常發出「呼呼」之類的聲音，二人沒有介意。

然後看完第三集。

「呵呵。」

我現在不是在看動畫片嗎。肩膀被搖動了。
中斷收看動畫後，夏爾可愛的臉出現在了眼前。

「兄長大人，任務，完成。」

「辛苦了。得救了。」

不，不，微笑著從夏爾那裡收到了上課申請文件。

「不確認內容可以嗎？」伊莉斯說。

「沒有必要。」

說實話，內容什麼的怎樣都好。

「沒想到兄長這麼信任我……我害羞了。」

實際上是害羞的夏爾。不，嗯。我當然信任你。

「那麼，兄長大人，我差不多要回去了。伊利斯小姐也過得好嗎？」

「哦。我不知道你來幹什麼，下次見。」

「嗯。夏爾也要注意身體。」

我敏捷地轉到伊利斯的背後，從背後用手遮住了他的眼睛。

「不在～…………消失！」

夏爾穿過設置在牆壁上的【任意門】，離開了就放開手。（門在使用前後看起來是牆壁）

「現在門的聲音，好像不是從門口，而是從牆壁傳來的一樣……？」

「在意細節的人，是不會出人頭地的？」

「唔，那可不好辦。因為我必須在這個國家掌握權力。」

真的，有這麼大的野心嗎？我沒興趣，不過，平民的這個傢伙那樣希望能理解。真是個野心家啊。

「啊，但是在意的話，會覺得各種各樣不可思議。你們的出生地是遠離王都的邊境吧？話雖如此，卻很輕鬆地走了過來，好像沒做過要事似的離開了，這是為什麼呢？」

伊利斯說個不停。

「說話的內容也不能理解。在涉及到稱讚你的話中，時常意味深長地說『圓桌』。是『黑暗的組織』、『樂園』、『幕後學生會』啦。要詳細瞭解的話，必須要成為騎士。我好像是那個候選人，但她到底站在什麼立場上，你到底是——。」

「所以不要在意細節。」

「嗯，雖然不能釋然，但是只能接受了……」

對著面無比為難的她，我委婉地提議。

「你也決定上課了吧？那麼，請馬上回去。」

但是伊利斯不動。

「你還沒決定所屬吧？難得來一次，一起考慮吧。如果沒有特別的希望的話，首先和我在同一個地方，怎麼樣呢？當然不會強迫你的。」

哪裡都行，只是那個眼鏡矮子博士倒是有點。

雖然有些人討厭強行勸誘，但只要聽她這麼一說，她就會被其他老師討厭。
應該阻止打算進入沒有前景的地方的朋友吧……。

「嗯？」

咦？但是等一下啊？
如果進入提亞教授的研究室，會被其他的老師給予壞印象？難道是為了毀掉研究室，而將所屬的學生趕到退學去嗎？那麼？

天啟了。

我把手放在伊利斯的肩上。

「其實我對古代魔法也有興趣。為了進入那個研究室，和擔當教授取得了聯繫。」

「原來如此。那麼，也可以推薦我一下嗎？老實說，成績不是很好，根據場合不同也有可能被拒絕。專門研究古代魔法的只有一個，無論如何我也想加入那裡。」

不，推薦什麼的沒必要吧？在我回應那個地方之前，平時是歡迎加入的狀態。
伊利斯抓著我搭在她肩上的手，真摯地凝視著。

「拜託了。如上所述。」

低下頭，將我的手放在額頭上。手和額頭都很溫暖。

「沒什麼。」

「謝謝。總是依賴你，真是對不起。我發誓總有一天會還這筆債的。」

不知怎麼的，好像被借了錢似的。

那麼，明天是去提交上課的申請文件，和所屬的拜訪吧。是換分身呢。先前約好隔天換班，所以沒辦法。
放在桌子上的美少女手辦，明明應該沒有意識，卻還盯著我。