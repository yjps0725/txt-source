第二天一早，在米蘭達來喊她起床之前，麻里子就醒了。不知為何，昨晚睡得出奇平穩，被子還好好地裹在身上。不過，似乎熟睡的時候出了很多汗，身上感覺潮乎乎的，像是被汗水浸透了一樣。靜臥在床上，她轉頭望向窗邊，紙窗外還是曙色未明，比米蘭達往常過來的時間要稍早了些。

意識迷迷朦朦之間，麻里子躺在床上思忖著，雖然還想再睡一會兒，不過米蘭達應該不久之後就會來了。這樣的話，也睡不踏實。難得醒得這麼早，不如這次去叫醒米蘭達，讓她好好吃驚一回。這麼想著，麻里子準備從床上起身⋯⋯

「啊，好疼！腰那裡──」

瞬間感到一陣鈍痛，她趕緊放平身體，重新躺回床上。

「哈啊──」

後背與床板接觸帶來的輕微衝擊，又激起一陣劇烈的疼痛。麻里子忍不住發出了呻吟。

（這是什麼啊？！肌肉痛嗎？）

稍一活動，痛楚就洶涌地襲來。她只得盡量保持平躺的姿勢，老老實實待在床上，並試圖在腦海裏找出頭緒。回想起來，昨晚洗完澡的時候就感覺腰有些沉⋯

昨天下午實在有些得意忘形了──揮舞著大鐮刀，把周圍的雜草統統清理了一遍。站在一旁的卡米魯都被她嚇壊了。反反覆覆彎腰的動作重複了一個多小時，估計肌肉痛就是這麼引起的。這樣想著，新的疑問很快便浮現出來。

（就算是那樣，對現在麻里子的身體來說，單純的肌肉痛會痛到這種程度？）

再之前呢？昨天上午一直站在廚房裡準備料理；早晨是和米蘭達晨練。然後⋯前天是狩獵野豬嗎？當時確實有些累了，但一覺醒來就恢復了啊。難道說還是有積累下來的疲勞嗎？

麻里子一面整理著思緒，一面靜靜感受著身體的反應。躺穩后，腰部的疼痛似乎有所緩和。但是，之前劇痛之下無暇顧及的諸多不適感也慢慢顯現出來。

首先是身體格外沉重酸脹。雖說起床太早的話也會有類似的感覺，可現在明顯要難受許多。其次是下腹部一直隱隱作痛。雖然不及腰痛劇烈，但也是相當難捱。這些症狀既不像是單純的感冒，又不像是吃了什麼不舒服的東西所致。

忽然間，麻里子的腦海裏浮現出了真理子的身姿。腰腹墜痛、頭昏倦怠──真理子從前的確偶爾會出現這些症狀。有時候，就算吃了藥，還是難受得只能躺在床上。那時，她總苦笑著稱那個為「月信的使者」

想到可能導致自己諸多不適的真正原因，麻里子額頭上突然冒出了冷汗。如果這真的是正確答案，那麼症狀肯定不止這些了⋯恐怕，身上各處感受到的潮氣，並非汗水之類的東西⋯麻里子強忍住腰上的鈍痛，把身體扭向一邊，想要趕緊察看之前躺過的位置。

「咕──」

昏暗的寢室內，白色床單中央浸染的暗紅色血漬清晰得刺眼。恍惚間，麻里子甚至聯想起漫畫裡處女的落紅（但量顯然沒有這麼多）。焦躁與慌亂很快充滿了她的內心。此時此刻，她仿彿尿床的小孩子一般不知所措。

「咿！疼疼疼⋯」

慌亂之間，又一陣強烈的痛感從腰部蔓延開來。諷刺的是，疼痛反倒讓麻里子得以冷靜下來。眼下，急躁只會帶來多餘的痛楚，還不如先想辦法解決症狀。於是她靜靜嘆了口氣，恢復成之前平躺的姿勢。

（不知道這裡有沒有止痛藥什麼的？⋯不不不，現在手頭沒有的東西就別想了。如果再這麼待下去，米蘭達很快就會過來了⋯）

要是這種情形被她看到的話，真要尷尬死了。所以不管怎麼說，這樣的結局一定要避免。

在昏暗的室內，即使多少有些暗視能力，麻里子還是抑制不住內心的不安感。先讓房間亮一些好了──這樣想著，她盡量保持著仰臥的姿勢，同時小心翼翼地控制著小臂的肌肉，將手一點點挪到朝向台燈的方向，然後默念出點亮的咒語。幸運的是，台燈順利啟動，房間頓時明亮起來。

（對了，用魔法的話，說不定會有辦法呢？）

當然，遊戲裡不可能會有止痛魔法，因為本來玩家就感覺不到遊戲角色的疼痛。但是治療受傷和疾病的魔法卻是存在的。而「麻里子」基本完全掌握了所有治療系的魔法。雖然來到這邊以後還一次都沒有用過，但就現在的情況來看不妨一試。

（不是病⋯嗯，內膜剝落導致的出血，算作受傷嗎？）

麻里子搜索記憶，找到了疼痛的起因。如果是受傷的話就有可以應付的魔法了。而且不需要直接接觸傷口，應該也會有效果──這樣想著，她把手輕輕搭在小腹上。

「治癒」

遊戲中的治癒是恢復HP的魔法。如果說HP減少的狀態對應現實中的受傷，那麼治癒就是促進傷口修復的魔法。

此外，在遊戲裡，受傷的同時體力亦會下降。如果只是稍有減少的話倒不會有太大的問題；但體力驟降的話，身體很快就會無法正常活動。因此在救助傷者時，治癒經常要與體力恢復魔法結合使用。

（好了⋯嗎？稍微緩解了一些？）

感知到從體內涌出的魔力，麻里子知道治癒魔法正常啟動了。「傷口」應該是痊癒了吧。下腹的疼痛似乎緩和了些，但是腰痛和倦怠感仍然沒有好轉。

（這麼說來，其它症狀不是由受傷直接導致的啊。我記得應該是⋯雌激素和什麼激素的平衡被打破⋯⋯要解決這個嗎？）

回想起來，真理子以前喝過的一種藥好像就是專門調節激素水平的。總而言之，這邊應該不太可能有相應的魔法。

（激素失調⋯⋯激素平衡紊亂⋯⋯激素水平異常。如果不是疾病，那應該算是一種⋯狀態異常嗎？那樣的話⋯）

「狀態恢復」

再次把手放在身體上，麻里子閉上眼睛，輕聲念出咒語。比剛才更加大量的魔力從體內涌出。

遊戲中的狀態恢復魔法，顧名思義，是消除異常狀態的魔法。在遊戲中，異常狀態包括直接的行動禁錮以及「麻痺」之類的負面狀態。

狀態恢復魔法順利啟動，麻里子緩緩睜開了眼睛。她試著直起上半身，側過頭，慢慢轉動脖頸。隨後，她長舒了一口氣。

「好像，奏效了吶⋯」


════════════════════

腰痛和疲勞感都消失了，

下次繼續。（笑）

如果真的有治療系魔法，感覺肯定會像這樣被應用起來吧。