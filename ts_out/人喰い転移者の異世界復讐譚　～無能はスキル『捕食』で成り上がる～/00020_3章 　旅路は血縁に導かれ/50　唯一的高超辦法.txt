「嗯嗯，果然打招呼是很重要的呢。和我想的一樣，大哥哥是個出色的人啊」

大概是對和我的對話感到很是滿意吧，芙蘭西思嘻嘻笑著，心情似乎很好。

「對攻擊過一次的對象，技能就不會再發揮效果了」，艾爾萊亞曾如此推測過的，而現在突然間就變得能看見她的身姿了，所以看來這個推測是正確的吧。

使用者的芙蘭西思，應該是利用著這個特性的。
然而她卻輕咬了我一下，故意顯現出了身影──即是說她沒有敵對的打算嗎？

「為甚麼⋯⋯明明至今為止都躲藏起來的，現在卻在我面前露面了？」
「因為我輸了遊戲」
「遊戲？」
「倍倍（拜拜）全部殺掉遊戲！每天把殺的人數翻倍，如果能把大家全部殺掉的話，就是我贏了。但是因為被妨礙了，所以是我輸了。啊—啊，如果在那個地方把在場的所有人都殺掉的話，今天應該就成功了，但是沒想到會被攻擊了」

雖然只是瞎猜的，但是原來那真的是遊戲嗎。
並且猜中了，她精神上未成熟的這一點。
芙蘭西思的年齡看起來至少比我小５歳，不夠成熟也是理所當然的。

「既然輸了，其實在那個時候就要停止殺戮，遊戲結束了。然後我必須離開這個城鎮，但是因為無論如何都想和大哥哥說說話，所以我就忍不住『咯噗』了大哥哥。沒辦法啊，畢竟察覺到我的存在的人，大哥哥可是第二個。果然是因為殺了很多人，所以能看到死亡的氣息吧？大哥哥的周圍也有死亡在盤旋著。基希尼亞也是一樣。不，也許大哥哥的是更加厲害的」

看來是很期待和我說話吧，芙蘭西思向我說個不停。
本來我就因為她的登場而混亂著，然後還有這個資訊量。
大腦是承受不住的。

「等等，芙蘭西思」
「叫芙蘭就可以了，如果是大哥哥的話」
「那麼，芙蘭，我可以一個一個地進行確認嗎？」
「嗯，如果是關於我的事的話，可以全部都告訴出來哦」

很坦率。
連殺氣都感覺不到。
但是，她的身旁擺著恐怕是殺了許多人的，沾滿血液，與芙蘭自己的身高差不多長的銀色的兇器。
最接近那種形狀的工具是斜口鉗嗎。
她是將它銳利的前端從背後刺進去，打開握柄部分來把身體撕成兩半的吧。

「首先，我想聽聽芙蘭的技能的說明」
「我知道了。我的技能是善惡的彼岸【Invisible】。這是常時發動型的技能，直到向對方施加攻擊為止，無論我做甚麼都不會被察覺到」
「常時發動型⋯⋯不能自己中斷嗎？」
「嗯，所以察覺到我的大哥哥是很出色的人啊♪」

察覺到芙蘭的存在的人，她說我是第二個。
如果說第一個，是我在王都交戰過的帝国的阿尼瑪使基希尼亞的話，那麼這孩子在此之前是怎麼活下來的呢。

「喂─，岬，你在聽嗎─？」
「從剛才開始就很安靜，怎麼了嗎？」

百合和艾爾萊亞找我攀談。
果然她們完全沒有注意到芙蘭的存在，好像認為我到現在為止都是一直沉默著。

「芙蘭，可以告訴大家你的事嗎？」
「不需要。有大哥哥在就夠了。只跟我說話吧」
「百合，艾爾萊亞，拉比。和大家的相遇，才能使我成為現在的我。如果沒有與大家相遇的話，也許我就沒能察覺到芙蘭的存在了」

雖然淨是單方面地捲入他們，並不是那麼美好的相遇呢。

「呣─，大哥哥真任性」
「對不起，但是只有這一點是不能讓步的」
「我明白了喲，那麼等一下吧」

芙蘭突然小跑接近百合，然後伴隨著「嘿」的吆喝聲用拳頭輕輕地打在她的臉頰上。

「嗚哇，甚麼，誰！？」

百合發出受到驚嚇的叫聲，差點從椅子上摔下來。
接著，芙蘭又給百合大腿上的艾爾萊亞一個小拳頭。

「呀！」

然後最後，芙蘭往拉比的小腿一踢。
咯，響起了低沉的聲音。

「好疼嗚嗚嗚⋯⋯！」

是不是唯獨對拉比太過分了？
但不管怎麼說，多虧了對全員的攻擊，大家似乎都能看見芙蘭的身姿了。
兩人的視線，都集中到了在我身旁的她。
拉比似乎還在揉著小腿痛苦著。

「岬，難道那孩子⋯⋯就是那個殺人鬼嗎」
「嗯，是哦。我是芙蘭西思・斯派塔韋萊思。因為大哥哥說無論如何都想要這樣子，所以我才讓你們看見我的」
「大哥哥，嗎？」

聽到「大哥哥」這個詞語後，百合和艾爾萊亞的視線都一齊轉向了拉比。

「不對哦，大哥哥說的是岬哥哥。才不是在那邊的叫拉比的小矮子」
「小腿被踢了，還被說小矮子⋯⋯」

拉比一個人受到了打擊。
反正馬上就會恢復過來的吧，所以先把他放著不管。
因為她稱我為「大哥哥」時稱呼得太自然了，所以沒有甚麼不協調感。
不，雖然本來叫我做大哥哥是正確的，但是現在的我可是女性啊。
我看向下方的兩個膨脹，再度進行確認。

「但是，為甚麼大哥哥胸前會有那種東西呢？身體又細，頭髮又長，臉也像女人一樣」

真的認為我是男的嗎。

「因為就是女的啊」

或許是因為無法單憑我的說話就接受，芙蘭緊緊地抓住了我的胸。
意外地痛。
就這樣揉搓了幾次後，她臉上露出吃驚的表情，看著我的臉說。

「是真東西啊」

那當然啊。
接著，芙蘭隔著褲子輕輕地觸摸我的胯間，又驚愕地說道。

「上面有，下面沒有！」

不用你說我也知道啊。

「好奇怪—啊─，明明我的感覺從來都沒有失誤過的。分明是沾著男人和殺人者的氣味的哦？」
「畢竟我原來是男的呢，不過由於發生了各種各樣的事就變成這樣的身體了」
「發生了各種各樣的事，就能變成女人的嗎？」

倒不如說那是我想問的。
如果可以透過魔法變成任何性別的話，我希望能變回男的呢。

「大哥哥⋯⋯不，大姐姐真是個不可思議的人啊。不是這麼不可思議的話便不能察覺到我的嗎」

將手放在下巴，歪著頭的芙蘭。
我否定我是特別的人。
如果不是變成這個身體的話，如果沒有成為阿尼瑪使的話，我就不會考慮復仇之類的事了，所以也不能說有錯吧。

「那麼，岬小姐，你和那個叫芙蘭西思的孩子說了些甚麼呢？」

拉比一邊揉著小腿一邊抱著若干的敵意瞪著芙蘭說道。

「因為想問的事情像山一樣多，所以目前正在進行提問當中。那麼，繼續剛才的提問，芙蘭是從帝国過來的嗎？」
「嗯。克里普托叔叔說，要我給王国添很多麻煩，於是把我送了出來」
「克里普托？」
「王国的人真是甚麼都不知道呢。是四將的其中之一哦，克里普托・扎福尼卡。是個對規則很固執的麻煩叔叔」

稱呼我為王国的人嗎，真是很微妙呢。
不過，在王都期間，我一次也沒有聽過四將之類的名稱。
艾薇與基希尼亞交戰的時候，也只說過她是將官級別，難道那只是在帝国中通用的俗稱嗎。

「也就是說，你受到那個叫做克里普托的男人的指示，為了攻擊王国而過來的嗎」
「與其說是指示，不如說是傳話吧。他說是基希尼亞想要我這樣做的。因為克里普托和基希尼亞的關係不好，所以我覺得這樣子傳話還真稀奇，但如果是和好了的話，那就是最好的了！」

好⋯⋯嗎。

「芙蘭西思醬，說不定那是⋯⋯」
「怎麼了？」
「你可能是被，叫克里普托的人騙了」
「騙了？為甚麼？」
「是為了令叫基希尼亞的那位變得孤立無援，吧」

艾爾萊亞把我的想法先一步說出來了。
一句話概括，就是趕走麻煩。
芙蘭雖然是四將之一，但實際上肯定是被當作基希尼亞的部下般的人的。
如果使用她的技能的話，暗殺就是小菜一碟。
她的存在，對於與基希尼亞對立的克里普托來說應該是極其棘手的。
但是，芙蘭有致命的弱點。
說到底她只不過是名年幼的少女而已。

「⋯⋯基希尼亞，難道是身陷危機嗎？」

艾爾萊亞重重地點了頭。
於是，芙蘭的臉色一下子發青了，一步，兩步地往後退。

「怎、怎怎、怎⋯⋯怎—辦！？我必須快點回到帝国了！啊，但是我也很在意大姐姐你們的事情啊！」
「說起來，既然芙蘭西思醬要回去帝国，那我們也拜託她來幫忙帶路不就好了嗎？畢竟殺了塔威爾納先生的也是這孩子」

確實如百合所說，如果芙蘭她是獨自一人從帝国過來的，那或許她能擔任帶路的角色。
但是，她的情況，也有可能是運用能力強行猛衝過來的。
有點擔心她能不能好好地帶路。

「塔威爾納，是誰？」
「是酒館的叔叔。原定是讓他來帶我們去帝国的。說起來，那個人可是帝国軍的人來著，你殺了他真的好嗎？」
「這麼說來，好像說過在蒙斯有伙伴來著⋯⋯我，可能惹基希尼亞生氣了⋯⋯」

眼看著就陷入消沉的芙蘭。
總覺得，光看這個樣子，是無法令人相信這孩子竟然慘殺了城鎮裡的人們的。
假如，沒有那些濺血以及沒有那個兇器的話。

「啊，但是如果我帶著大姐姐你們過去的話，基希尼亞也許會很高興的！」

從剛才開始就基希尼亞基希尼亞的，看來是很喜歡她呢。
大概是，代替了姐姐的角色吧。

「這樣決定了的話，就趕快殺了這個城鎮的人們後出發吧！」
「有殺掉的必要嗎？雖然我個人來說，那樣做也沒有關係」
「並不是我想殺的哦。既然遊戲已經輸了，對我來說就到此為止了。但是，大姐姐你們想幫助那個叫索蕾優的人對吧？」
「那件事你也聽到了啊」

一點私隱都沒有。
進入蒙斯之後的對話，有可能全都被芙蘭偷聽了嗎。
確實很便利，我能理解基希尼亞不放開她的理由。

「但是，為甚麼殺死城鎮裡的人們可以幫到索蕾優呢？」
「那人除了復仇以外甚麼都沒有，所以無論是現在就把商人公會的人們全都殺死來結束復仇，又或者是知道了真相後殺死福特金和拉蔻莎等人，不管是哪邊都不是良策啊」
「就是這樣啊」
「那麼很簡單啊，只要創造新的生存價值給她就行了！」

芙蘭兩手插在腰間，得意地「哼哼」笑著。
如果能這麼簡單就創造出生存價值的話，誰都不會感到煩惱和操心了。

「生存價值，比如說？」
「對殺害了城鎮的人們的，大姐姐你們進行復仇哦」

一瞬間不知所措，稍微過了一會之後──「啊啊，這樣啊」，奇妙地接受了。
即使復仇結束了，只要有新的復仇，索蕾優就能活下去。
如果那個對象是自己的話，確實是很簡單的事情。
只要把城鎮的人們都殺了，就可以滿足條件。
其實這樣的王国城鎮，我從最初開始就想把它毀滅的，而且也可以吃到阿尼瑪。

一石二鳥⋯⋯不，甚至是在此之上嗎。

「等等啊，芙蘭西思醬，這樣做的話，這次就會輪到我們被追殺了」
「芙蘭西思，醬？」
「⋯⋯芙蘭西思，小姐」

非常高壓地對待拉比的芙蘭。

「為甚麼只有我被這樣對待啊」，儘管如此抱怨著，拉比還是修訂了措辭。

「反正要去帝国，所以她是追不到過來的。只要，她不借助軍隊和王国的力量的話♪」

沒有任何後盾的人，要穿過国境是很困難的。
而且，如果索蕾優要加入軍隊的話。
預先把她的存在通知給普拉娜絲的話，便能為因奧利哈魯鋼的事而被逼到絶境的普拉娜絲派去援軍。

再加上──

「拉比，關於索蕾優的父母的死亡事件的資料，如果她看到了⋯⋯裡面有能讓她看一眼就會相信的內容嗎？」
「資料上面蓋上了證明這是正式文件的印章，我想蒙斯出身的索蕾優小姐是知道這個印章的」

──那樣的話，很好。

嗯，非常好。

「等、等一下啊，岬小姐，難道真的打算殺光城鎮裡的人嗎？這樣就會變成索蕾優小姐的敵人了哦？」
「這樣就好，倒不如說反而沒有不殺的理由了」

我不由得嘴角上揚。
單純地對殺害王国的人感到期待，而且僅僅只是殺了他們，我就能得到好處。
沒有比這更令人高興的事了。

「謝謝芙蘭，歸功於你，我已經沒有煩惱了」
「我，了不起嗎？」
「嗯，了不起了不起」

被誇獎之後，芙蘭就像撒嬌一樣地向這邊伸出了頭，我就如她所願撫摸她。
頭髮的柔軟觸感纏繞在手指上。
芙蘭心情很好地「嗯呼呼─」笑了起來。

這樣子撫摸了芙蘭的頭一陣子──然後我注視到百合和艾爾萊亞正盯著這邊。
我不由得浮現出苦笑，從芙蘭的頭上抽開了手，按照百合、艾爾萊亞的順序依次撫摸兩人的頭。
真是的，明明我也沒有誇獎、只是無意義地撫摸著，有甚麼高興呢。
話雖如此，但是我也在笑著。

「拉比君也要給岬摸一下嗎？」
「不用客氣！」

看著拚命拒絶的拉比，我們哈哈地大笑了。

在日本的時候，無論是在家裡還是班級裡都沒有感受到這樣的氣氛。
唯獨是，和姐姐──命，或是和彩花兩人獨處的時候，心情會變得放鬆，內心會變得平靜。
現在和那個又不一樣。

令人舒適的安心感，只有我的居所，正是因為成為了殺人犯──


◇◇◇

不久後太陽落山了。

百合哼著歌顯得十分高興。
艾爾萊亞的臉頰出現紅暈，像是發情了。
芙蘭如跳舞般跳著走著。
然後我，思念馳騁於充滿希望的未來。

人們沉睡的夜晚。
四位阿尼瑪使，為了宣告蒙斯城鎮的終結，得意洋洋地走出了旅館。


════════════════════

註釋：

【１】「倍倍」，意為加倍，其原文「倍々」的讀音為「ばいばい」，與Bye-Bye（拜拜）同音，是故這個遊戲的名稱是在玩諧音。

【２】Invisible，英語，有看不見的、隱形的之意。而技能名「善惡的彼岸」，便是第４６話的標題的所指。